#ifndef COMMON_H_
#define COMMON_H_

// Enable exceptions
#define __CL_ENABLE_EXCEPTIONS

// Set OpenCL version
#define CL_TARGET_OPENCL_VERSION 200

// Needed to work around compilation error with properties argument
// for cl::CommandQueue constructror using CUDA CL/cl.hpp
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS 1

// Needed to work around compilation errors with
// Khronos opencl.hpp (renamed to cl.hpp)
#define CL_HPP_ENABLE_EXCEPTIONS
#define CL_HPP_TARGET_OPENCL_VERSION 200

#include <chrono>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>

#include <CL/cl.hpp>

#define BUILD_LOG 1

typedef struct
{
    std::string deviceName;
    std::string driverVersion;

    unsigned numCUs;
    unsigned maxWGSize;
    unsigned maxAllocSize;
    unsigned maxGlobalSize;
    unsigned maxClockFreq;

    bool doubleSupported;
    cl_device_type deviceType;

    int computeWgsPerCU;

} device_info_t;

unsigned roundToPowOf2(unsigned number, int maxPower = 0);

void run(cl::Context &context, cl::Device &device, cl::CommandQueue &queue, device_info_t &devInfo);

cl::Program compile(const char *filename, cl::Context &context, cl::Device &device);

double run_kernel(cl::CommandQueue &queue, cl::Kernel &kernel, cl::NDRange &globalSize, cl::NDRange &localSize);

void report(std::string name, double runtime, double gflops, double gbytes);

void report_flops(std::string name, double runtime, float gflops);

void report_bytes(std::string name, double runtime, float gbytes);

#endif // end define COMMON_H
