#include "common.h"

unsigned roundToPowOf2(unsigned number, int maxPower)
{
    double logd = log(number) / log(2);
    logd = floor(logd);

    if (maxPower > 0)
    {
        logd = std::min(logd, (static_cast<double>(maxPower)));
    }

    return static_cast<unsigned>(pow(2, logd));
}

device_info_t getDeviceInfo(cl::Device &d)
{
    device_info_t devInfo;

    devInfo.deviceName = d.getInfo<CL_DEVICE_NAME>();
    devInfo.driverVersion = d.getInfo<CL_DRIVER_VERSION>();

    devInfo.numCUs = (unsigned)d.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>();
    std::vector<size_t> maxWIPerDim;
    maxWIPerDim = d.getInfo<CL_DEVICE_MAX_WORK_ITEM_SIZES>();
    devInfo.maxWGSize = (unsigned)maxWIPerDim[0];

    // Limiting max work-group size
    const unsigned int maxWGSize = 256;
    devInfo.maxWGSize = std::min(devInfo.maxWGSize, maxWGSize);

    devInfo.maxAllocSize = (unsigned)d.getInfo<CL_DEVICE_MAX_MEM_ALLOC_SIZE>();
    devInfo.maxGlobalSize = (unsigned)d.getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>();
    devInfo.maxClockFreq = (unsigned)d.getInfo<CL_DEVICE_MAX_CLOCK_FREQUENCY>();
    devInfo.doubleSupported = false;

    std::string extns = d.getInfo<CL_DEVICE_EXTENSIONS>();
    if ((extns.find("cl_khr_fp64") != std::string::npos) ||
        (extns.find("cl_amd_fp64") != std::string::npos))
    {
        devInfo.doubleSupported = true;
    }

    devInfo.deviceType = d.getInfo<CL_DEVICE_TYPE>();

    if (devInfo.deviceType & CL_DEVICE_TYPE_CPU)
    {
        devInfo.computeWgsPerCU = 512;
    }
    else
    { // GPU
        devInfo.computeWgsPerCU = 2048;
    }

    return devInfo;
}

double run_kernel(cl::CommandQueue &queue, cl::Kernel &kernel, cl::NDRange &globalSize, cl::NDRange &localSize)
{
    double runtime = 0;

    // Dummy calls
    queue.enqueueNDRangeKernel(kernel, cl::NullRange, globalSize, localSize);
    queue.finish();

    char *char_nr_iterations = getenv("NR_ITERATIONS");
    int nr_iterations = char_nr_iterations ? atoi(char_nr_iterations) : 10;

    for (int i = 0; i < nr_iterations; i++)
    {
        cl::Event event;
        queue.enqueueNDRangeKernel(kernel, cl::NullRange, globalSize, localSize, NULL, &event);
        queue.finish();
        cl_ulong start = event.getProfilingInfo<CL_PROFILING_COMMAND_START>();
        cl_ulong end = event.getProfilingInfo<CL_PROFILING_COMMAND_END>();
        std::chrono::nanoseconds duration(end - start);
        runtime += duration.count() * 1e-6;
        if (runtime > 1e3) {
            nr_iterations = i + 1;
            break;
        }
    }

    return runtime / nr_iterations;
}

cl::Program compile(const char *filename, cl::Context &context, cl::Device &device)
{
    std::ifstream source_file(filename);
    std::string source(std::istreambuf_iterator<char>(source_file),
                       (std::istreambuf_iterator<char>()));
    source_file.close();
    cl::Program program(context, source);
    try
    {
        char *char_opencl_flags = getenv("OPENCL_FLAGS");
        std::stringstream flags;
        if (char_opencl_flags)
        {
            flags << char_opencl_flags;
        }
        std::vector<cl::Device> devices{device};
        program.build(devices, flags.str().c_str());
    }
    catch (cl::Error e)
    {
#if BUILD_LOG
        std::cout << "Build log: " << std::endl;
        std::cout << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device);
        std::cout << std::endl;
#endif
    }
    return program;
}

void report(std::string name, double runtime, double gflops, double gbytes)
{
    unsigned width = 10;
    std::cout << std::setw(width) << std::string(name) << ": ";
    std::cout << std::setprecision(2) << std::fixed;
    std::cout << std::setw(width) << runtime << " ms";
    if (gflops != 0)
        std::cout << ", " << std::setw(width) << gflops / (runtime * 1e3) << " GFLOPS";
    if (gbytes != 0)
        std::cout << ", " << std::setw(width) << gbytes / (runtime * 1e3) << " GB/s";
    if (gflops != 0 && gbytes != 0)
    {
        float arithmetic_intensity = gflops / gbytes;
        std::cout << ", " << std::setw(width) << arithmetic_intensity << " Flop/byte";
    }
    std::cout << std::endl;
}

void report_flops(std::string name, double runtime, float gflops)
{
    report(name, runtime, gflops, 0);
}

void report_bytes(std::string name, double runtime, float gbytes)
{
    report(name, runtime, 0, gbytes);
}

int main(int argc, char **argv)
{
    // Set number of times to repeat the benchmark
    char *char_nr_repetitions = getenv("NR_REPETITIONS");
    int nr_repetitions = char_nr_repetitions ? atoi(char_nr_repetitions) : 1;

    // Set OpenCL platform to use
    char *char_platform_name = getenv("PLATFORM_NAME");

    // Set OpenCL device to use
    char *char_device_name = getenv("DEVICE_NAME");

    // Get platforms
    std::vector<cl::Platform> platforms;
    cl::Platform::get(&platforms);

    // Iterate all platforms
    for (int p = 0; p < platforms.size(); p++)
    {
        cl::Platform platform = platforms[p];
        std::string platform_name = platform.getInfo<CL_PLATFORM_NAME>();
        std::cout << ">> Platform: " << platform_name;

        if (char_platform_name && platform_name.compare(char_platform_name) != 0) {
            std::cout << " (skipped)" << std::endl;
            continue;
        } else {
            std::cout << std::endl;
        }

        // Get devices
        std::vector<cl::Device> devices;
        platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);

        // Iterate all devices
        for (int d = 0; d < devices.size(); d++) {
            // Get device
            cl::Device device = devices[d];
            device_info_t device_info = getDeviceInfo(device);

            // Print device info
            std::cout << ">> Device: " << device_info.deviceName;
            if (char_device_name && device_info.deviceName.compare(char_device_name) != 0) {
                std::cout << " (skipped)" << std::endl;
                continue;
            } else {
                std::cout << std::endl;
            }
            std::cout << "\tDevice type     : " << device_info.deviceType << std::endl;
            std::cout << "\tDriver version  : " << device_info.driverVersion << std::endl;
            std::cout << "\tCompute units   : " << device_info.numCUs << std::endl;
            std::cout << "\tClock frequency : " << device_info.maxClockFreq << " MHz" << std::endl;
            std::cout << std::endl;

            // Get context
            cl::Context ctx(device);

            // Get command queue
            cl::CommandQueue queue = cl::CommandQueue(ctx, device, CL_QUEUE_PROFILING_ENABLE);

            try
            {
                for (int i = 0; i < nr_repetitions; i++)
                {
                    run(ctx, device, queue, device_info);
                }
            }
            catch (cl::Error error)
            {
                std::cerr << "OpenCL error: "
                          << error.what() << "(" << error.err() << ")"
                          << std::endl;
            }
        } // end for device
    } // end for platform

    return EXIT_SUCCESS;
}
