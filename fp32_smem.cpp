#include "common.h"

void run(cl::Context &context, cl::Device &device, cl::CommandQueue &queue, device_info_t &devInfo)
{
    // Compile kernels
    cl::Program program = compile("kernels/fp32_smem.cl", context, device);

    // Parameters
    unsigned workPerWI = 512 * 2; // Flops executed per work-item
    unsigned globalWIs = (devInfo.numCUs) * (devInfo.computeWgsPerCU) * (devInfo.maxWGSize);
    uint32_t gflops = ((float)globalWIs * workPerWI) / 1e3f;
    uint32_t gbytes = ((float)globalWIs * workPerWI * sizeof(float)) / 1e3f;
    int maxWGSize = devInfo.maxWGSize;
    unsigned t = std::min(static_cast<unsigned int>(globalWIs * sizeof(cl_float)), devInfo.maxAllocSize);
    t = roundToPowOf2(t);
    globalWIs = t / sizeof(cl_float);
    double runtime;

    // Kernel dimensions
    cl::NDRange globalSize = globalWIs;
    cl::NDRange localSize = maxWGSize;

    // Output buffer
    cl::Buffer outputBuf = cl::Buffer(context, CL_MEM_WRITE_ONLY, (globalWIs * sizeof(cl_float)));

    // Get kernels
    cl::Kernel kernel_v1(program, "compute_sp_ai_v1");
    kernel_v1.setArg(0, outputBuf);

    cl::Kernel kernel_v2(program, "compute_sp_ai_v2");
    kernel_v2.setArg(0, outputBuf);

    cl::Kernel kernel_v3(program, "compute_sp_ai_v3");
    kernel_v3.setArg(0, outputBuf);

    cl::Kernel kernel_v4(program, "compute_sp_ai_v4");
    kernel_v4.setArg(0, outputBuf);

    cl::Kernel kernel_v5(program, "compute_sp_ai_v5");
    kernel_v5.setArg(0, outputBuf);

    cl::Kernel kernel_v6(program, "compute_sp_ai_v6");
    kernel_v6.setArg(0, outputBuf);

    cl::Kernel kernel_v7(program, "compute_sp_ai_v7");
    kernel_v7.setArg(0, outputBuf);

    // Run kernels
    runtime = run_kernel(queue, kernel_v1, globalSize, localSize);
    report("0.25", runtime, gflops * 1, gbytes);

    runtime = run_kernel(queue, kernel_v2, globalSize, localSize);
    report("0.50", runtime, gflops * 2, gbytes);

    runtime = run_kernel(queue, kernel_v3, globalSize, localSize);
    report("1.00", runtime, gflops * 4, gbytes);

    runtime = run_kernel(queue, kernel_v4, globalSize, localSize);
    report("1.25", runtime, gflops * 5, gbytes);

    runtime = run_kernel(queue, kernel_v5, globalSize, localSize);
    report("2.00", runtime, gflops * 10, gbytes);

    runtime = run_kernel(queue, kernel_v6, globalSize, localSize);
    report("4.00", runtime, gflops * 16, gbytes);

    runtime = run_kernel(queue, kernel_v7, globalSize, localSize);
    report("5.00", runtime, gflops * 20, gbytes);
}
