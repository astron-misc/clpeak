#include "common.h"

void run(cl::Context &context, cl::Device &device, cl::CommandQueue &queue, device_info_t &devInfo)
{
    // Compile kernels
    cl::Program program = compile("kernels/fp32_sincos_native.cl", context, device);

    // Parameters
    unsigned globalWIs = devInfo.numCUs * devInfo.computeWgsPerCU * devInfo.maxWGSize;
    unsigned maxWGSize = devInfo.maxWGSize;
    int nr_repetitions = 8;
    int nr_iterations = 8192;
    double gflops = (devInfo.numCUs * devInfo.maxWGSize) * (1ULL * nr_repetitions * nr_iterations * 4);

    // Kernel dimensions
    cl::NDRange globalSize = globalWIs;
    cl::NDRange localSize = maxWGSize;

    // Output buffer
    cl::Buffer outputBuf = cl::Buffer(context, CL_MEM_WRITE_ONLY, (globalWIs * sizeof(cl_float)));

    // Get kernels
    cl::Kernel kernel_1_8(program, "fp32_sincos_1_8"); kernel_1_8.setArg(0, outputBuf);
    cl::Kernel kernel_1_4(program, "fp32_sincos_1_4"); kernel_1_4.setArg(0, outputBuf);
    cl::Kernel kernel_1_2(program, "fp32_sincos_1_2"); kernel_1_2.setArg(0, outputBuf);
    cl::Kernel kernel_1_1(program, "fp32_sincos_1_1"); kernel_1_1.setArg(0, outputBuf);
    cl::Kernel kernel_2_1(program, "fp32_sincos_2_1"); kernel_2_1.setArg(0, outputBuf);
    cl::Kernel kernel_4_1(program, "fp32_sincos_4_1"); kernel_4_1.setArg(0, outputBuf);
    cl::Kernel kernel_8_1(program, "fp32_sincos_8_1"); kernel_8_1.setArg(0, outputBuf);
    cl::Kernel kernel_16_1(program, "fp32_sincos_16_1"); kernel_16_1.setArg(0, outputBuf);
    cl::Kernel kernel_32_1(program, "fp32_sincos_32_1"); kernel_32_1.setArg(0, outputBuf);
    cl::Kernel kernel_64_1(program, "fp32_sincos_64_1"); kernel_64_1.setArg(0, outputBuf);
    cl::Kernel kernel_128_1(program, "fp32_sincos_128_1"); kernel_128_1.setArg(0, outputBuf);

    // Run kernels
    double runtime;
    runtime = run_kernel(queue, kernel_128_1, globalSize, localSize);
    report_flops(" 128:1", runtime, gflops);

    runtime = run_kernel(queue, kernel_64_1, globalSize, localSize);
    report_flops("  64:1", runtime, gflops);

    runtime = run_kernel(queue, kernel_32_1, globalSize, localSize);
    report_flops("  32:1", runtime, gflops);

    runtime = run_kernel(queue, kernel_16_1, globalSize, localSize);
    report_flops("  16:1", runtime, gflops);

    runtime = run_kernel(queue, kernel_8_1, globalSize, localSize);
    report_flops("   8:1", runtime, gflops);

    runtime = run_kernel(queue, kernel_4_1, globalSize, localSize);
    report_flops("   4:1", runtime, gflops);

    runtime = run_kernel(queue, kernel_2_1, globalSize, localSize);
    report_flops("   2:1", runtime, gflops);

    runtime = run_kernel(queue, kernel_1_1, globalSize, localSize);
    report_flops("   1:1", runtime, gflops);

    runtime = run_kernel(queue, kernel_1_2, globalSize, localSize);
    report_flops("   1:2", runtime, gflops);

    runtime = run_kernel(queue, kernel_1_4, globalSize, localSize);
    report_flops("   1:4", runtime, gflops);

    runtime = run_kernel(queue, kernel_1_8, globalSize, localSize);
    report_flops("   1:8", runtime, gflops);
}