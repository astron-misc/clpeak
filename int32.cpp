#include "common.h"

void run(cl::Context &context, cl::Device &device, cl::CommandQueue &queue, device_info_t &devInfo)
{
    // Compile kernels
    cl::Program program = compile("kernels/int32.cl", context, device);

    // Parameters
    unsigned workPerWI = 4096; // Flops executed per work-item
    unsigned globalWIs = (devInfo.numCUs) * (devInfo.computeWgsPerCU) * (devInfo.maxWGSize);
    uint32_t gflops = ((float)globalWIs * workPerWI) / 1e3f;
    unsigned maxWGSize = devInfo.maxWGSize;
    unsigned t = std::min(static_cast<unsigned int>(globalWIs * sizeof(cl_float)), devInfo.maxAllocSize);
    t = roundToPowOf2(t);
    globalWIs = t / sizeof(cl_float);
    cl_int A = 1;
    double runtime;

    // Output buffer
    cl::Buffer outputBuf = cl::Buffer(context, CL_MEM_WRITE_ONLY, (globalWIs * sizeof(cl_int)));

    // Kernel dimensions
    cl::NDRange globalSize = globalWIs;
    cl::NDRange localSize = maxWGSize;

    // Get kernels
    cl::Kernel kernel_v1(program, "compute_integer_v1");
    kernel_v1.setArg(0, outputBuf), kernel_v1.setArg(1, A);

    cl::Kernel kernel_v2(program, "compute_integer_v2");
    kernel_v2.setArg(0, outputBuf), kernel_v2.setArg(1, A);

    cl::Kernel kernel_v4(program, "compute_integer_v4");
    kernel_v4.setArg(0, outputBuf), kernel_v4.setArg(1, A);

    cl::Kernel kernel_v8(program, "compute_integer_v8");
    kernel_v8.setArg(0, outputBuf), kernel_v8.setArg(1, A);

    cl::Kernel kernel_v16(program, "compute_integer_v16");
    kernel_v16.setArg(0, outputBuf), kernel_v16.setArg(1, A);

    // Run kernels
    // Vector width 1
    runtime = run_kernel(queue, kernel_v1, globalSize, localSize);
    report_flops("int", runtime, gflops);

    // Vector width 2
    runtime = run_kernel(queue, kernel_v2, globalSize, localSize);
    report_flops("int2", runtime, gflops);

    // Vector width 4
    runtime = run_kernel(queue, kernel_v4, globalSize, localSize);
    report_flops("int4", runtime, gflops);

    // Vector width 8
    runtime = run_kernel(queue, kernel_v8, globalSize, localSize);
    report_flops("int8", runtime, gflops);

    // Vector width 16
    runtime = run_kernel(queue, kernel_v16, globalSize, localSize);
    report_flops("int16", runtime, gflops);
}
