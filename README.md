# clpeak
Based on https://github.com/krrishnarraj/clpeak

`clpeak` is a collection of OpenCL benchmarks:
- `fp32.x`: measure 32-bit FMA floating-point performance
- `fp64.x`: measure 64-bit FMA floating-point performance
- `int32.x`: measure 32-bit FMA integer performance
- `fp32_sincos.x`: measure performance for instruction mixes of 32-bit floating-point FMA and sine/cosine operations
- `fp32_dmem.x`: measure performance for instruction mixes of 32-bit floating-point FMA and device memory load operations
- `fp32_smem.x`: measure performance for instruction mixes of 32-bit floating-point FMA and local memory load operations

# build instructions
`clpeak` can be build using CMake:
```
mkdir build
cd build
cmake ..
make
```
This assumes that the OpenCL development files are installed system-wide, or that you loaded an enviroment-modules (`module load ...`) to setup your environment beforehand.

Alternatively, you can manually specifiy the location of your OpenCL files:
```
cmake .. -DOpenCL_LIBRARY=<prefix>/lib/libOpenCL.so -DOpenCL_INCLUDE_DIR=<path-to>/include/CL/cl.h -DOpenCL_CPP_INCLUDE_DIR=<path-to>/CL/cl.hpp
```

If these files are not found, CMake uses the scripts in `<srcdir>/scripts` to install/download the required files in your build directory.
Note that the `OpenCL_LIBRARY` should be a full path including `libOpenCL.so`, while the include directories should point to the directory containing the `CL` subdirectory.

## Build using AMD ROCm
CMake will not automatically detect an AMD ROCm installation. Assuming ROCm is installed in `/opt/rocm`, use:
```
AMDAPPSDKROOT=/opt/rocm/opencl cmake ..
make
```

## Build on Intel DevCloud
On the Intel DevCloud, the OpenCL runtime is preconfigured but no OpenCL development files are present.
You can let CMake download the required files to build `clpeak` for you by running:
```
cmake .. -DUSE_INTEL_SDK=1
make
```

# docker
To run `clpeak` on a sytem with no OpenCL driver installed, you may find it convient to use Docker.

Based on https://github.com/chihchun/opencl-docker, we created a simple Docker file to setup OpenCL for Intel CPUs and GPUs on Ubuntu 18.04.

It can be built as follows:
```
docker build -f ./docker/Dockerfile.intel . -t opencl
```

Running the docker image requires passing through the `/dev/dri` devices (these are for integrated Intel GPU):
```
docker run -ti --rm --device /dev/dri:/dev/dri opencl
```
This will run `clinfo`, which should show your OpenCL capable Intel CPU and/or GPU.

To run `clpeak` in this container, mount the current directory, e.g.:
```
docker run -ti --rm --device /dev/dri:/dev/dri -v$(pwd):/clpeak -w /clpeak opencl bash
```

Now you can proceed with building `clpeak` (see above) and running the benchmark.

Note that this Docker image does not come with OpenCL SDK and/or OpenCL (C++) header files pre-installed. It has only been tested on systems with Intel CPU and onboard Intel Graphics.

# known issues
- The status of the `int32.x`,`fp32_dmem.x` and `fp32_smem.x` benchmark are unknown. These are not built by default.
- On an AMD CPU, `fp32.x` reports incorrect (too high) performance for `float2`, `float4`, `float8` and `float16`. Since the `fp32_sincos_*.x` benchmarks also use `float2`, the reported performance is likely also too high.
