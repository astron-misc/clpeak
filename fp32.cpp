#include "common.h"

void run(cl::Context &context, cl::Device &device, cl::CommandQueue &queue, device_info_t &devInfo)
{
    // Compile kernels
    cl::Program program = compile("kernels/fp32.cl", context, device);

    // Parameters
    unsigned globalWIs = devInfo.numCUs * devInfo.computeWgsPerCU * devInfo.maxWGSize;
    unsigned maxWGSize = devInfo.maxWGSize;
    cl_float A = 1.3f;
    double runtime;
    int nr_iterations = 8;
    double gflops = (devInfo.numCUs * devInfo.maxWGSize) * (1ULL * nr_iterations * 8192 * 4);

    // Kernel dimensions
    cl::NDRange globalSize = globalWIs;
    cl::NDRange localSize = maxWGSize;

    // Output buffer
    cl::Buffer outputBuf = cl::Buffer(context, CL_MEM_WRITE_ONLY, (globalWIs * sizeof(cl_float)));

    // Get kernels
    cl::Kernel kernel_v1(program, "compute_sp_v1");
    kernel_v1.setArg(0, outputBuf), kernel_v1.setArg(1, A);

    cl::Kernel kernel_v2(program, "compute_sp_v2");
    kernel_v2.setArg(0, outputBuf), kernel_v2.setArg(1, A);

    cl::Kernel kernel_v4(program, "compute_sp_v4");
    kernel_v4.setArg(0, outputBuf), kernel_v4.setArg(1, A);

    cl::Kernel kernel_v8(program, "compute_sp_v8");
    kernel_v8.setArg(0, outputBuf), kernel_v8.setArg(1, A);

    cl::Kernel kernel_v16(program, "compute_sp_v16");
    kernel_v16.setArg(0, outputBuf), kernel_v16.setArg(1, A);

    // Run kernels
    // Vector width 1
    runtime = run_kernel(queue, kernel_v1, globalSize, localSize);
    report_flops("float", runtime, gflops);

    // Vector width 2
    runtime = run_kernel(queue, kernel_v2, globalSize, localSize);
    report_flops("float2", runtime, gflops);

    // Vector width 4
    runtime = run_kernel(queue, kernel_v4, globalSize, localSize);
    report_flops("float4", runtime, gflops);

    // Vector width 8
    runtime = run_kernel(queue, kernel_v8, globalSize, localSize);
    report_flops("float8", runtime, gflops);

    // Vector width 16
    runtime = run_kernel(queue, kernel_v16, globalSize, localSize);
    report_flops("float16", runtime, gflops);
}
