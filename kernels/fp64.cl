#if defined(cl_khr_fp64)
  #pragma OPENCL EXTENSION cl_khr_fp64 : enable
  #define DOUBLE_AVAILABLE
#elif defined(cl_amd_fp64)
  #pragma OPENCL EXTENSION cl_amd_fp64 : enable
  #define DOUBLE_AVAILABLE
#endif

#ifdef DOUBLE_AVAILABLE

#define NR_ITERATIONS 8

double mad1(int n, double x, double y) {
    for (int i = 0; i < n; i++) {
        x = mad(y, x, y);
        y = mad(x, y, x);
        x = mad(y, x, y);
        y = mad(x, y, x);
    }
    return x;
}


double2 mad2(int n, double2 x, double2 y) {
    for (int i = 0; i < n; i++) {
        x = mad(y, x, y);
        y = mad(x, y, x);
        x = mad(y, x, y);
        y = mad(x, y, x);
    }
    return x;
}


double4 mad4(int n, double4 x, double4 y) {
    for (int i = 0; i < n; i++) {
        x = mad(y, x, y);
        y = mad(x, y, x);
        x = mad(y, x, y);
        y = mad(x, y, x);
    }
    return x;
}


double8 mad8(int n, double8 x, double8 y) {
    for (int i = 0; i < n; i++) {
        x = mad(y, x, y);
        y = mad(x, y, x);
        x = mad(y, x, y);
        y = mad(x, y, x);
    }
    return x;
}


double16 mad16(int n, double16 x, double16 y) {
    for (int i = 0; i < n; i++) {
        x = mad(y, x, y);
        y = mad(x, y, x);
        x = mad(y, x, y);
        y = mad(x, y, x);
    }
    return x;
}


__kernel void compute_dp_v1(__global double *ptr, double _A)
{
    double x = _A;
    double y = (double)get_local_id(0);

    for (int i = 0; i < NR_ITERATIONS; i++) {
        y = mad1(2048, x, y);
    }

    ptr[get_global_id(0)] = y;
}


__kernel void compute_dp_v2(__global double *ptr, double _A)
{
    double2 x = (double2)(_A, (_A+1));
    double2 y = (double2)get_local_id(0);

    for (int i = 0; i < NR_ITERATIONS; i++) {
        y = mad2(1024, x, y);
    }

    ptr[get_global_id(0)] = (y.S0) + (y.S1);
}


__kernel void compute_dp_v4(__global double *ptr, double _A)
{
    double4 x = (double4)(_A, (_A+1), (_A+2), (_A+3));
    double4 y = (double4)get_local_id(0);

    for (int i = 0; i < NR_ITERATIONS; i++) {
        y = mad4(512, x, y);
    }

    ptr[get_global_id(0)] = (y.S0) + (y.S1) + (y.S2) + (y.S3);
}


__kernel void compute_dp_v8(__global double *ptr, double _A)
{
    double8 x = (double8)(_A, (_A+1), (_A+2), (_A+3), (_A+4), (_A+5), (_A+6), (_A+7));
    double8 y = (double8)get_local_id(0);

    for (int i = 0; i < NR_ITERATIONS; i++) {
        y = mad8(256, x, y);
    }

    ptr[get_global_id(0)] = (y.S0) + (y.S1) + (y.S2) + (y.S3) + (y.S4) + (y.S5) + (y.S6) + (y.S7);
}


__kernel void compute_dp_v16(__global double *ptr, double _A)
{
    double16 x = (double16)(_A, (_A+1), (_A+2), (_A+3), (_A+4), (_A+5), (_A+6), (_A+7),
                    (_A+8), (_A+9), (_A+10), (_A+11), (_A+12), (_A+13), (_A+14), (_A+15));
    double16 y = (double16)get_local_id(0);

    for (int i = 0; i < NR_ITERATIONS; i++) {
        y = mad16(128, x, y);
    }

    double2 t = (y.S01) + (y.S23) + (y.S45) + (y.S67) + (y.S89) + (y.SAB) + (y.SCD) + (y.SEF);
    ptr[get_global_id(0)] = t.S0 + t.S1;
}

#endif      // DOUBLE_AVAILABLE
