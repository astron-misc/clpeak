#define NR_REPETITIONS 8
#define NR_ITERATIONS 8192

float2 _fp32_sincos_1_1(int nr_fma, int nr_sincos, float2 a, float2 b)
{
    for (int i = 0; i < nr_fma; i++) {
        a.x = mad(a.x, b.x, b.y);
    }
    for (int i = 0; i < nr_sincos; i++) {
        a.x = sin(a.x);
        a.y = cos(a.x);

    }
    return a;
}

__kernel void fp32_sincos_1_8(__global float *ptr)
{
    float2 a = (float2)(get_global_id(0), get_global_id(0)+1);
    float2 b = (float2)(1, 2);
    float2 c = (float2)(3, 4);

    for (int i = 0; i < NR_REPETITIONS; i++) {
        for (int j = 0; j < NR_ITERATIONS; j++) {
            a = _fp32_sincos_1_1(1, 8, a, b);
        }
    }

    ptr[get_global_id(0)] = a.x + a.y + b.x + b.y + c.x + c.y;
}

__kernel void fp32_sincos_1_4(__global float *ptr)
{
    float2 a = (float2)(get_global_id(0), get_global_id(0)+1);
    float2 b = (float2)(1, 2);
    float2 c = (float2)(3, 4);

    for (int i = 0; i < NR_REPETITIONS; i++) {
        for (int j = 0; j < NR_ITERATIONS; j++) {
            a = _fp32_sincos_1_1(1, 4, a, b);
        }
    }

    ptr[get_global_id(0)] = a.x + a.y + b.x + b.y + c.x + c.y;
}

__kernel void fp32_sincos_1_2(__global float *ptr)
{
    float2 a = (float2)(get_global_id(0), get_global_id(0)+1);
    float2 b = (float2)(1, 2);
    float2 c = (float2)(3, 4);

    for (int i = 0; i < NR_REPETITIONS; i++) {
        for (int j = 0; j < NR_ITERATIONS; j++) {
            a = _fp32_sincos_1_1(1, 2, a, b);
        }
    }

    ptr[get_global_id(0)] = a.x + a.y + b.x + b.y + c.x + c.y;
}

__kernel void fp32_sincos_1_1(__global float *ptr)
{
    float2 a = (float2)(get_global_id(0), get_global_id(0)+1);
    float2 b = (float2)(1, 2);
    float2 c = (float2)(3, 4);

    for (int i = 0; i < NR_REPETITIONS; i++) {
        for (int j = 0; j < NR_ITERATIONS; j++) {
            a = _fp32_sincos_1_1(1, 1, a, b);
        }
    }

    ptr[get_global_id(0)] = a.x + a.y + b.x + b.y + c.x + c.y;
}

__kernel void fp32_sincos_2_1(__global float *ptr)
{
    float2 a = (float2)(get_global_id(0), get_global_id(0)+1);
    float2 b = (float2)(1, 2);
    float2 c = (float2)(3, 4);

    for (int i = 0; i < NR_REPETITIONS; i++) {
        for (int j = 0; j < NR_ITERATIONS/2; j++) {
            a = _fp32_sincos_1_1(2, 1, a, b);
        }
    }

    ptr[get_global_id(0)] = a.x + a.y + b.x + b.y + c.x + c.y;
}

__kernel void fp32_sincos_4_1(__global float *ptr)
{
    float2 a = (float2)(get_global_id(0), get_global_id(0)+1);
    float2 b = (float2)(1, 2);
    float2 c = (float2)(3, 4);

    for (int i = 0; i < NR_REPETITIONS; i++) {
        for (int j = 0; j < NR_ITERATIONS/4; j++) {
            a = _fp32_sincos_1_1(4, 1, a, b);
        }
    }

    ptr[get_global_id(0)] = a.x + a.y + b.x + b.y + c.x + c.y;
}

__kernel void fp32_sincos_8_1(__global float *ptr)
{
    float2 a = (float2)(get_global_id(0), get_global_id(0)+1);
    float2 b = (float2)(1, 2);
    float2 c = (float2)(3, 4);

    for (int i = 0; i < NR_REPETITIONS; i++) {
        for (int j = 0; j < NR_ITERATIONS/8; j++) {
            a = _fp32_sincos_1_1(8, 1, a, b);
        }
    }

    ptr[get_global_id(0)] = a.x + a.y + b.x + b.y + c.x + c.y;
}

__kernel void fp32_sincos_16_1(__global float *ptr)
{
    float2 a = (float2)(get_global_id(0), get_global_id(0)+1);
    float2 b = (float2)(1, 2);
    float2 c = (float2)(3, 4);

    for (int i = 0; i < NR_REPETITIONS; i++) {
        for (int j = 0; j < NR_ITERATIONS/16; j++) {
            a = _fp32_sincos_1_1(16, 1, a, b);
        }
    }

    ptr[get_global_id(0)] = a.x + a.y + b.x + b.y + c.x + c.y;
}

__kernel void fp32_sincos_32_1(__global float *ptr)
{
    float2 a = (float2)(get_global_id(0), get_global_id(0)+1);
    float2 b = (float2)(1, 2);
    float2 c = (float2)(3, 4);

    for (int i = 0; i < NR_REPETITIONS; i++) {
        for (int j = 0; j < NR_ITERATIONS/32; j++) {
            a = _fp32_sincos_1_1(32, 1, a, b);
        }
    }

    ptr[get_global_id(0)] = a.x + a.y + b.x + b.y + c.x + c.y;
}

__kernel void fp32_sincos_64_1(__global float *ptr)
{
    float2 a = (float2)(get_global_id(0), get_global_id(0)+1);
    float2 b = (float2)(1, 2);
    float2 c = (float2)(3, 4);

    for (int i = 0; i < NR_REPETITIONS; i++) {
        for (int j = 0; j < NR_ITERATIONS/64; j++) {
            a = _fp32_sincos_1_1(64, 1, a, b);
        }
    }

    ptr[get_global_id(0)] = a.x + a.y + b.x + b.y;
    ptr[get_global_id(0)] = a.x + a.y + b.x + b.y + c.x + c.y;
}

__kernel void fp32_sincos_128_1(__global float *ptr)
{
    float2 a = (float2)(get_global_id(0), get_global_id(0)+1);
    float2 b = (float2)(1, 2);
    float2 c = (float2)(3, 4);

    for (int i = 0; i < NR_REPETITIONS; i++) {
        for (int j = 0; j < NR_ITERATIONS/128; j++) {
            a = _fp32_sincos_1_1(128, 1, a, b);
        }
    }

    ptr[get_global_id(0)] = a.x + a.y + b.x + b.y + c.x + c.y;
}
