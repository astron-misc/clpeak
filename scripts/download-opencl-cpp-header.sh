#!/bin/bash

ROOT_DIR=$(pwd)/include/CL
mkdir -p ${ROOT_DIR}
wget -q --no-check-certificate --directory-prefix=${ROOT_DIR} https://raw.githubusercontent.com/KhronosGroup/OpenCL-CLHPP/main/include/CL/opencl.hpp -O ${ROOT_DIR}/cl.hpp
