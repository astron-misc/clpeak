#!/bin/bash

# Intel SDK for OpenCL Applications
# 	https://www.intel.com/content/www/us/en/developer/tools/opencl-sdk/overview.html
INTEL_OPENCL_SDK=intel_sdk_for_opencl_applications_2020.1.395.tar.gz
INTEL_OPENCL_SDK_URL=http://registrationcenter-download.intel.com/akdlm/irc_nas/vcp/16625/

# Temporary and installation directories
TEMP_DIR=/tmp/intel-opencl-sdk
ROOT_DIR=$(pwd)
INSTALL_DIR=${ROOT_DIR}/opt/intel-opencl-sdk

# Install the Intel OpenCL SDK
if [ ! -d ${INSTALL_DIR} ]
then
    mkdir -p ${TEMP_DIR}
    mkdir -p ${INSTALL_DIR}
    mkdir -p ${TEMP_DIR}
    cd ${TEMP_DIR}
    if [ ! -f ${TEMP_DIR}/${INTEL_OPENCL_SDK} ]
    then
        mkdir -p ${TEMP_DIR}
        echo "Downloading:" ${INTEL_OPENCL_SDK}
        wget -q  ${INTEL_OPENCL_SDK_URL}${INTEL_OPENCL_SDK}
        echo "Extracting to:" ${TEMP_DIR}
        tar xf $INTEL_OPENCL_SDK
    else
        echo ${INTEL_OPENCL_SDK} "already downloaded."
    fi
    echo "CONTINUE_WITH_OPTIONAL_ERROR=yes" > silent.cfg
    echo "Installing in:" ${INSTALL_DIR}
    ./intel_sdk_for_opencl_applications_*/install.sh -s silent.cfg --install_dir $INSTALL_DIR --accept_eula &> /dev/null
    cd ${ROOT_DIR}
else
    echo "Intel OpenCL SDK already installed."
fi

# Provide symlinks for header files
mkdir -p ${ROOT_DIR}/include
cp -r ${INSTALL_DIR}/system_studio_*/opencl/SDK/include/* ${ROOT_DIR}/include/

# Provide symlinks for libraries
mkdir -p ${ROOT_DIR}/lib
cp -r ${INSTALL_DIR}/system_studio_*/opencl/SDK/lib64/* ${ROOT_DIR}/lib/
