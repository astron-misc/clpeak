#include "common.h"

void populate(float *ptr, unsigned N)
{
    srand((unsigned int)time(NULL));

    for (int i = 0; i < (int)N; i++)
    {
        ptr[i] = (float)rand();
    }
}

void run(cl::Context& context, cl::Device& device, cl::CommandQueue& queue, device_info_t& devInfo)
{
    // Compile kernels
    cl::Program program = compile("kernels/fp32_dmem.cl", context, device);

    // Variables
    unsigned fetchPerWI = 16;
    cl_uint maxItems = devInfo.maxAllocSize / sizeof(float) / 2;
    cl_uint numItems;
    double runtime_lo;
    double runtime_go;
    double runtime;

    // Set an upper-limit for cpu devies
    if (devInfo.deviceType & CL_DEVICE_TYPE_CPU)
    {
        numItems = roundToPowOf2(maxItems, 25);
    }
    else
    {
        numItems = roundToPowOf2(maxItems);
    }
    uint32_t gbytes = ((float)numItems * sizeof(float));

    // Allocate buffers
    float *arr = new float[numItems];
    populate(arr, numItems);

    cl::Buffer inputBuf = cl::Buffer(context, CL_MEM_READ_ONLY, (numItems * sizeof(float)));
    cl::Buffer outputBuf = cl::Buffer(context, CL_MEM_WRITE_ONLY, (numItems * sizeof(float)));
    queue.enqueueWriteBuffer(inputBuf, CL_TRUE, 0, (numItems * sizeof(float)), arr);

    // Kernel dimensions
    cl::NDRange globalSize;
    cl::NDRange localSize = devInfo.maxWGSize;

    // Get kernels
    cl::Kernel kernel_v1_lo(program, "global_bandwidth_v1_local_offset");
    kernel_v1_lo.setArg(0, inputBuf), kernel_v1_lo.setArg(1, outputBuf);

    cl::Kernel kernel_v2_lo(program, "global_bandwidth_v2_local_offset");
    kernel_v2_lo.setArg(0, inputBuf), kernel_v2_lo.setArg(1, outputBuf);

    cl::Kernel kernel_v4_lo(program, "global_bandwidth_v4_local_offset");
    kernel_v4_lo.setArg(0, inputBuf), kernel_v4_lo.setArg(1, outputBuf);

    cl::Kernel kernel_v8_lo(program, "global_bandwidth_v8_local_offset");
    kernel_v8_lo.setArg(0, inputBuf), kernel_v8_lo.setArg(1, outputBuf);

    cl::Kernel kernel_v16_lo(program, "global_bandwidth_v16_local_offset");
    kernel_v16_lo.setArg(0, inputBuf), kernel_v16_lo.setArg(1, outputBuf);

    cl::Kernel kernel_v1_go(program, "global_bandwidth_v1_global_offset");
    kernel_v1_go.setArg(0, inputBuf), kernel_v1_go.setArg(1, outputBuf);

    cl::Kernel kernel_v2_go(program, "global_bandwidth_v2_global_offset");
    kernel_v2_go.setArg(0, inputBuf), kernel_v2_go.setArg(1, outputBuf);

    cl::Kernel kernel_v4_go(program, "global_bandwidth_v4_global_offset");
    kernel_v4_go.setArg(0, inputBuf), kernel_v4_go.setArg(1, outputBuf);

    cl::Kernel kernel_v8_go(program, "global_bandwidth_v8_global_offset");
    kernel_v8_go.setArg(0, inputBuf), kernel_v8_go.setArg(1, outputBuf);

    cl::Kernel kernel_v16_go(program, "global_bandwidth_v16_global_offset");
    kernel_v16_go.setArg(0, inputBuf), kernel_v16_go.setArg(1, outputBuf);

    // Run 2 kind of bandwidth kernels
    // lo -- local_size offset - subsequent fetches at local_size offset
    // go -- global_size offset
    // Vector width 1
    globalSize = numItems / fetchPerWI;
    runtime_lo = run_kernel(queue, kernel_v1_lo, globalSize, localSize);
    runtime_go = run_kernel(queue, kernel_v1_go, globalSize, localSize);
    runtime = std::min(runtime_lo, runtime_go);
    report_bytes("float", runtime, gbytes);

    // Vector width 2
    globalSize = (numItems / 2 / fetchPerWI);
    runtime_lo = run_kernel(queue, kernel_v2_lo, globalSize, localSize);
    runtime_go = run_kernel(queue, kernel_v2_go, globalSize, localSize);
    runtime = std::min(runtime_lo, runtime_go);
    report_bytes("float2", runtime, gbytes);

    // Vector width 4
    globalSize = (numItems / 4 / fetchPerWI);
    runtime_lo = run_kernel(queue, kernel_v4_lo, globalSize, localSize);
    runtime_go = run_kernel(queue, kernel_v4_go, globalSize, localSize);
    runtime = std::min(runtime_lo, runtime_go);
    report_bytes("float4", runtime, gbytes);

    // Vector width 8
    globalSize = (numItems / 8 / fetchPerWI);
    runtime_lo = run_kernel(queue, kernel_v8_lo, globalSize, localSize);
    runtime_go = run_kernel(queue, kernel_v8_go, globalSize, localSize);
    runtime = std::min(runtime_lo, runtime_go);
    report_bytes("float8", runtime, gbytes);

    // Vector width 16
    globalSize = (numItems / 16 / fetchPerWI);
    runtime_lo = run_kernel(queue, kernel_v16_lo, globalSize, localSize);
    runtime_go = run_kernel(queue, kernel_v16_go, globalSize, localSize);
    runtime = std::min(runtime_lo, runtime_go);
    report_bytes("float16", runtime, gbytes);

    delete[] arr;
}
